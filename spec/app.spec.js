const { getUserRandomNumber, welcomeMessage } = require('../app');

describe("App getUserRandomNumber", function() {
  
    it("is > 1000", async () => {
      const randomNumber = await getUserRandomNumber();
  
      expect(randomNumber).toBeGreaterThan(1000);
      // expect(randomNumber).toBeLessThan(500);      







      

    });
});

describe("User welcome message", function() {
  const number = 1500;

  it("has to include a specific string", async () => {
    const messageString = "Welcome. You're client number is:";
    const userMessage = await welcomeMessage();

    expect(userMessage).toContain(messageString);
  });
});